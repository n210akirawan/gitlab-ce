import canceledSvg from 'icons/_icon_status_canceled.svg';
import createdSvg from 'icons/_icon_status_created.svg';
import failedSvg from 'icons/_icon_status_failed.svg';
import manualSvg from 'icons/_icon_status_manual.svg';
import pendingSvg from 'icons/_icon_status_pending.svg';
import runningSvg from 'icons/_icon_status_running.svg';
import skippedSvg from 'icons/_icon_status_skipped.svg';
import successSvg from 'icons/_icon_status_success.svg';
import warningSvg from 'icons/_icon_status_warning.svg';

import canceledBorderlessSvg from 'icons/_icon_status_canceled_borderless.svg';
import createdBorderlessSvg from 'icons/_icon_status_created_borderless.svg';
import failedBorderlessSvg from 'icons/_icon_status_failed_borderless.svg';
import manualBorderlessSvg from 'icons/_icon_status_manual_borderless.svg';
import pendingBorderlessSvg from 'icons/_icon_status_pending_borderless.svg';
import runningBorderlessSvg from 'icons/_icon_status_running_borderless.svg';
import skippedBorderlessSvg from 'icons/_icon_status_skipped_borderless.svg';
import successBorderlessSvg from 'icons/_icon_status_success_borderless.svg';
import warningBorderlessSvg from 'icons/_icon_status_warning_borderless.svg';

export const statusClassToSvgMap = {
  icon_status_canceled: canceledSvg,
  icon_status_created: createdSvg,
  icon_status_failed: failedSvg,
  icon_status_manual: manualSvg,
  icon_status_pending: pendingSvg,
  icon_status_running: runningSvg,
  icon_status_skipped: skippedSvg,
  icon_status_success: successSvg,
  icon_status_warning: warningSvg,
};

export const statusClassToBorderlessSvgMap = {
  icon_status_canceled: canceledBorderlessSvg,
  icon_status_created: createdBorderlessSvg,
  icon_status_failed: failedBorderlessSvg,
  icon_status_manual: manualBorderlessSvg,
  icon_status_pending: pendingBorderlessSvg,
  icon_status_running: runningBorderlessSvg,
  icon_status_skipped: skippedBorderlessSvg,
  icon_status_success: successBorderlessSvg,
  icon_status_warning: warningBorderlessSvg,
};
